#include <iostream>
#include <cstdlib>
#include <ctime>

class Stack
{
public:
	Stack(int a)
	{
		size = a;
		stack = new int[a];
		
		//filling an array with random numbers 
		srand(time(NULL));
		for (int i = 0; i < a; i++)
		{
			stack[i] = rand();
		}
	}

	int Size()
	{
		return size;
	}

	int Push(int i)
	{
		return this->stack[size++] = i;
	}

	void Print()
	{
		for (int i = 0; i < size; i++)
		{
			std::cout << stack[i] << " ";
		}
		std::cout << '\n';
	}

	bool IsEmpty()
	{
		if (size == 0)
			std::cout << "Stack is empty!\n";
		else
			std::cout << "Stack is not empty!\n";

		return 0;
	}
	int Pop()
	{
		size--;
		return this -> stack[size];
	}

	int Head()
	{
		return stack[size-1];
	}
private:
	
	int size;
	int* stack;
	int i;
};

int main()
{
	int a;
	int i;

	std::cout << "Enter a stack size (a pozitive number): ";
	std::cin >> a;
	Stack T(a);

	std::cout << "A stack size: " << T.Size() << '\n';

	T.IsEmpty();

	std::cout << "Elements of the stack: ";
	T.Print();

	std::cout << "A head element of the stack : " << T.Head() << '\n';

	std::cout << "Pop a head element of the stack : " << T.Pop() << '\n';

	std::cout << "A stack size: " << T.Size() << '\n';

	T.IsEmpty();

	std::cout << "Elements of the stack: ";
	T.Print();

	std::cout << "A head element of the stack : " << T.Head() << '\n';

	std::cout << "Pop a head element of the stack : " << T.Pop() << '\n';

	std::cout << "A stack size: " << T.Size() << '\n';

	T.IsEmpty();

	std::cout << "Elements of the stack: ";
	T.Print();

	std::cout << "A head element of the stack : " << T.Head() << '\n';

	std::cout << "Pop a head element of the stack : " << T.Pop() << '\n';

	T.IsEmpty();

	std::cout << "Enter an element's stack: ";
	std::cin >> i;
	T.Push(i);

	std::cout << "A stack size: " << T.Size() << '\n';

	T.IsEmpty();

	std::cout << "Elements of the stack: ";
	T.Print();

	std::cout << "A head element of the stack : " << T.Head() << '\n';

	std::cout << "Enter an element's stack: ";
	std::cin >> i;
	T.Push(i);

	std::cout << "A stack size: " << T.Size() << '\n';

	T.IsEmpty();

	std::cout << "Elements of the stack: ";
	T.Print();

	std::cout << "A head element of the stack : " << T.Head() << '\n'; std::cout << "Enter an element's stack: ";
	std::cin >> i;
	T.Push(i);

	std::cout << "A stack size: " << T.Size() << '\n';

	T.IsEmpty();

	std::cout << "Elements of the stack: ";
	T.Print();

	std::cout << "A head element of the stack : " << T.Head() << '\n';
	return 0;
}